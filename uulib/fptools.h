/*
 * fptools.c, some helper functions for getcgi.c and uu(en|de)view
 *
 * Distributed under the terms of the GNU General Public License.
 * Use and be happy.
 */

/*
 * Some handy, nonstandard functions. Note that the original may
 * be both faster and better. ``better'', if your compiler allows
 * cleaner use of such functions by proper use of ``const''.
 *
 * $Id$
 */

#ifndef FPTOOLS_H__
#define FPTOOLS_H__

#include <string.h>
#include <stdint.h>

typedef signed char schar;
typedef unsigned char uchar;

#include "ecb.h"

#ifndef TOOLEXPORT
#define TOOLEXPORT
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if HAVE_GETC_UNLOCKED
# define FP_getc(s) getc_unlocked (s)
#else
# define FP_getc(s) getc (s)
#endif

#if HAVE_FEOF_UNLOCKED
# define FP_feof(s) feof_unlocked (s)
#else
# define FP_feof(s) feof (s)
#endif

#if HAVE_FERROR_UNLOCKED
# define FP_ferror(s) ferror_unlocked (s)
#else
# define FP_ferror(s) ferror (s)
#endif

#if HAVE_FLOCKFILE
# define FP_flockfile(s) flockfile (s)
#else
# define FP_flockfile(s) ((void *)0)
#endif

#define FP_strstr(a,b) strstr (a, b)
#define FP_strerror(a) strerror (a)

void	TOOLEXPORT	FP_free		(void *);
char *	TOOLEXPORT	FP_strdup	(char *);
char *	TOOLEXPORT	FP_strncpy	(char *, char *, int);
void *	TOOLEXPORT	FP_memdup	(const void *, int);
int	TOOLEXPORT	FP_stricmp	(const char *, const char *);
int	TOOLEXPORT	FP_strnicmp	(const char *, const char *, int);
char *	TOOLEXPORT	FP_strrstr	(const char *, const char *);
char *	TOOLEXPORT	FP_stoupper	(char *);
char *	TOOLEXPORT	FP_stolower	(char *);
int	TOOLEXPORT	FP_strmatch	(char *, char *);
char *	TOOLEXPORT	FP_stristr	(char *, char *);
char *	TOOLEXPORT	FP_strirstr	(char *, char *);
char *	TOOLEXPORT	FP_strrchr	(const char *, int);
char *	TOOLEXPORT	FP_fgets	(char *, int, FILE *);
char *	TOOLEXPORT	FP_strpbrk	(char *, char *);
char *	TOOLEXPORT	FP_strtok	(char *, char *);
char *	TOOLEXPORT	FP_cutdir	(char *);

/* like stgricmp, but only works when one of the strings is printable ascii */
/* not containing any of these characters: @`[\]^:_{|}~ */
int	TOOLEXPORT	FP_strnicmp_fast (const char *, const char *, int);

#if 0 /* API differs too much between systems to make those replacements */
#if HAVE_STRCASECMP
# define FP_stricmp(a,b) strcasecmp (a, b)
#endif

#if HAVE_STRNCASECMP
# define FP_strnicmp(a,b,l) strncasecmp (a, b, l)
#endif

#if HAVE_STRCASESTR
# define FP_stristr(a,b) strcasestr (a, b)
#endif
#endif

/* this hashes in the final 0 octet to avoid a branch - might or might not be worth it */
/* this also means that this function distinguishes between NULL and "" */
static uint32_t
fnv1a (const char *str)
{
  uint32_t hash = 0x811C9DC5, c;

  if (str)
    for (;;)
      {
        c = *str++;
        hash = (hash ^ c) * 16777619;
        if (!c)
          break;
      }

  return hash;
}

#ifdef __cplusplus
}
#endif
#endif

